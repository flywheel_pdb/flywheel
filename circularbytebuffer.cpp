// circularbytebuffer.cpp
// Copyright (C) 2010-2011 Andrew Munkres
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA


#include <string>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "circularbytebuffer.h"
#include "minmax.h"

CircularByteBuffer::CircularByteBuffer(size_t bufsize):
  m_bufsize(bufsize),
  readpoint(0),
  writepoint(0),
  //buffer(new char[bufsize])
  buffer(NULL),
  m_trace(false)
{
  // fprintf(stderr, "DEBUG: new cbb at %#llx (buf at %#llx)\n", this, buffer);
}

CircularByteBuffer::CircularByteBuffer():
  m_bufsize(0),
  readpoint(0),
  writepoint(0),
  buffer(NULL),
  m_trace(false)
{
  // fprintf(stderr, "DEBUG: new cbb at %#llx (no buf)\n", this, buffer);
  // fprintf(stderr, "Will this really ever be called?\n");
  fprintf(stderr, "Internal error: no-params version of CircularByteBuffer constructor was called, which shouldn't happen.\n");
  abort();
}

CircularByteBuffer::CircularByteBuffer(const CircularByteBuffer &orig):
  m_bufsize(orig.m_bufsize),
  //readpoint(orig.readpoint),
  readpoint(0),
  //writepoint(orig.writepoint),
  writepoint(orig.amount()),
  //buffer(new char[m_bufsize])
  buffer(NULL),
  m_trace(orig.m_trace)
{
  // NOTE: although read method isn't declared const, it is effectively const *if* you pass NO_MODIFY; this means that it's ok to do the following const_cast.
  if (0 != writepoint)
  {
    delayedInit();
    const_cast<CircularByteBuffer *>(&orig)->read(buffer, writepoint, NO_MODIFY);
  }
  // fprintf(stderr, "DEBUG: copy old cbb %#llx (buf at %#llx) to new cbb %#llx (buf at %#llx)\n", &orig, orig.buffer, this, buffer);
}

CircularByteBuffer::~CircularByteBuffer()
{
  // fprintf(stderr, "DEBUG: deleting cbb at %#llx (buf at %#llx)\n", this, buffer);
  if (NULL != buffer)
  {
    delete[] buffer;
    buffer = NULL;
  }
}

// NOTE: when empty, writepoint == readpoint
//       when full, (writepoint + 1) % bufsize == readpoint

size_t CircularByteBuffer::amount() const
{
  if (writepoint == readpoint)
    return 0;
  if (writepoint > readpoint)
    return writepoint - readpoint;
  else // writepoint < readpoint
    return (m_bufsize - readpoint) + writepoint;
}

size_t CircularByteBuffer::write(const char *in, size_t max)
{
  if (NULL == buffer)
    delayedInit();
  
  if (m_trace)
    fprintf(stderr, "DEBUG: write to CircularByteBuffer, in contains \"%s\", buffer initially contains \"%s\", readpoint is %lld, writepoint is %lld, amount() is %lld\n", in, std::string(buffer, m_bufsize).c_str(), readpoint, writepoint, amount());
  
  size_t free_space((m_bufsize - 1) - amount()), write_amount(minimum<size_t>(max, free_space)),
  amount_until_end(m_bufsize - writepoint);
  
  if (write_amount >= amount_until_end)
  {
    size_t amount_from_beginning(write_amount - amount_until_end);
    memcpy(buffer + writepoint, in, amount_until_end);
    memcpy(buffer, in + amount_until_end, amount_from_beginning);
    writepoint = amount_from_beginning;
  }
  else
  {
    memcpy(buffer + writepoint, in, write_amount);
    writepoint += write_amount;
  }
  
  if (m_trace)
    fprintf(stderr, "DEBUG: after write to CircularByteBuffer, buffer contains \"%s\", readpoint is %lld, writepoint is %lld, amount() is %lld\n", std::string(buffer, m_bufsize).c_str(), readpoint, writepoint, amount());
  
  return write_amount;
}

size_t CircularByteBuffer::read(char *out, size_t max, unsigned int flags)
{
  if (m_trace)
  {
    fprintf(stderr, "DEBUG: read from CircularByteBuffer, flags is %#x, out initially contains \"%s\", buffer initially contains \"%s\", readpoint is %lld, writepoint is %lld, amount() is %lld\n", flags, ((NULL == out) ? "**** out is NULL ****" : std::string(out, m_bufsize).c_str()), ((NULL == buffer) ? "**** buffer is NULL ****" : std::string(buffer, m_bufsize).c_str()), readpoint, writepoint, amount());
    fprintf(stderr, "DEBUG: (%#x & %#x) is %#x; (!(%#x & %#x)) is %#x\n", flags, NO_COPY, (flags & NO_COPY), flags, NO_COPY, (!(flags & NO_COPY)));
    fprintf(stderr, "DEBUG: (%#x & %#x) is %#x; (!(%#x & %#x)) is %#x\n", flags, NO_MODIFY, (flags & NO_MODIFY), flags, NO_MODIFY, (!(flags & NO_MODIFY)));
  }
  
  size_t used_space(amount()), read_amount(minimum<size_t>(max, used_space)), amount_until_end(m_bufsize - readpoint);
  
  if (m_trace)
    fprintf(stderr, "DEBUG: used_space is %llu, read_amount is %llu, amount_until_end is %llu\n", used_space, read_amount, amount_until_end);
  
  if (read_amount >= amount_until_end)
  {
    size_t amount_from_beginning(read_amount - amount_until_end);
    
    if (m_trace)
      fprintf(stderr, "DEBUG: in (read_amount >= amount_until_end) branch; amount_from_beginning is %llu\n", amount_from_beginning);
    
    if (!(flags & NO_COPY))
    {
      if (m_trace)
        fprintf(stderr, "DEBUG: we did not get NO_COPY flag, so we will write output\n");
      
      if (m_trace)
        fprintf(stderr, "DEBUG: writing this to out: \"%s\"\n", std::string(buffer + readpoint, amount_until_end).c_str());
      
      memcpy(out, buffer + readpoint, amount_until_end);
      
      if (m_trace)
        fprintf(stderr, "DEBUG: writing this to (out + %lld): \"%s\"\n", amount_until_end, std::string(buffer, amount_from_beginning).c_str());
      
      memcpy(out + amount_until_end, buffer, amount_from_beginning);
    }
    if (!(flags & NO_MODIFY))
    {
      if (m_trace)
        fprintf(stderr, "DEBUG: we did not get NO_MODIFY flag, so we will dequeue\n");
      
      readpoint = amount_from_beginning;
      
      if (m_trace)
        fprintf(stderr, "DEBUG: readpoint is now %llu\n", readpoint);
    }
  }
  else
  {
    if (m_trace)
      fprintf(stderr, "DEBUG: in (read_amount < amount_until_end) branch\n");
    
    if (!(flags & NO_COPY))
    {
      if (m_trace)
        fprintf(stderr, "DEBUG: we did not get NO_COPY flag, so we will write output\n");
      
      if (m_trace)
        fprintf(stderr, "DEBUG: writing this to out: \"%s\"\n", std::string(buffer + readpoint, read_amount).c_str());
      
      memcpy(out, buffer + readpoint, read_amount);
    }
    if (!(flags & NO_MODIFY))
    {
      if (m_trace)
        fprintf(stderr, "DEBUG: we did not get NO_MODIFY flag, so we will dequeue\n");
      
      readpoint += read_amount;
      
      if (m_trace)
        fprintf(stderr, "DEBUG: readpoint is now %llu\n", readpoint);
    }
  }
  
  if (m_trace)
    fprintf(stderr, "DEBUG: after read from CircularByteBuffer, out contains \"%s\", buffer contains \"%s\", readpoint is %lld, writepoint is %lld, amount() is %lld\n", ((NULL == out) ? "**** out is NULL ****" : std::string(out, m_bufsize).c_str()), ((NULL == buffer) ? "**** buffer is NULL ****" : std::string(buffer, m_bufsize).c_str()), readpoint, writepoint, amount());
  
  return read_amount;
}

void CircularByteBuffer::clear()
{
  readpoint = 0;
  writepoint = 0;
}

size_t CircularByteBuffer::maxAmount() const
{
  return m_bufsize - 1;
}

void CircularByteBuffer::delayedInit()
{
  if (m_trace)
    fprintf(stderr, "DEBUG: doing CircularByteBuffer::delayedInit() with size %llu\n", m_bufsize);
  buffer = new char[m_bufsize];
}

void CircularByteBuffer::setTrace(bool trace)
{
  m_trace = trace;
}
