// flywheel.cpp
// Copyright (C) 2011-2012 Andrew Munkres
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>

#include <iostream>

#include <event2/event.h>

#include "circularbytebuffer.h"

using namespace std;

struct SharedStuff
{
  CircularByteBuffer *c_buffer;
  char *b_buffer;
  bool empty;
  event *ev_out_writeable;
  int in_fd, out_fd;
};

void notifyQuit(int);
void doIn(int fd, short, void *shared_v);
void doOut(int fd, short, void *shared_v);
void doQuit(int, short, void *shared_v);
void flush(const SharedStuff *shared);
int main(int argc, char **argv);

static const ssize_t CBUFFER_SIZE(16777216);
static const ssize_t BBUFFER_SIZE(131072);

static int sig_fds[2];

void notifyQuit(int)
{
//   cerr << "DEBUG: entering doIn" << endl;
  write(sig_fds[1], "B", 1);
}

void doIn(int fd, short, void *shared_v)
{
//   cerr << "DEBUG: entering doIn" << endl;
  SharedStuff *shared(static_cast<SharedStuff *>(shared_v));
  ssize_t to_store_count(read(fd, shared->b_buffer, BBUFFER_SIZE));
  if (-1 == to_store_count)
  {
    cerr << "READ ERROR! (" << strerror(errno) << ")" << endl;
    return;
  }
  else if (0 == to_store_count)
  {
//     cerr << "Reached end of input; " << shared->c_buffer->amount() << " bytes remaining to be sent." << endl;
    flush(shared);
  }
//   else
//     cerr << "DEBUG: read " << to_store_count << " bytes" << endl;
//   cerr << "DEBUG: cbuf is currently " << (shared->empty ? "empty" : "not empty") << endl;
  size_t stored_count(shared->c_buffer->write(shared->b_buffer, to_store_count));
//   cerr << "DEBUG: stored " << to_store_count << " bytes in cbuf" << endl;  
  if (to_store_count > stored_count)
    cerr << "OVERRUN! Lost " << (to_store_count - stored_count) << " bytes!" << endl;
  if (shared->empty && (stored_count > 0))
  {
    shared->empty = false;
    event_add(shared->ev_out_writeable, NULL);
//     cerr << "DEBUG: turned ON watching for able-to-write" << endl;
  }
}

void doOut(int fd, short, void *shared_v)
{
//   cerr << "DEBUG: entering doOut" << endl;
  SharedStuff *shared(static_cast<SharedStuff *>(shared_v));
  size_t to_send_count(shared->c_buffer->read(shared->b_buffer, BBUFFER_SIZE, NO_MODIFY));
//   cerr << "DEBUG: peeked at " << to_send_count << " bytes in cbuf" << endl;  
  ssize_t sent_count(write(fd, shared->b_buffer, to_send_count));
  if (-1 == sent_count)
  {
    cerr << "WRITE ERROR! (" << strerror(errno) << ")" << endl;
    return;
  }
  else
//     cerr << "DEBUG: wrote " << sent_count << " bytes and erased them from cbuf" << endl;
  shared->c_buffer->read(NULL, sent_count, NO_COPY);
//   cerr << "DEBUG: " << shared->c_buffer->amount() << " bytes remain in cbuf" << endl;
  if (0 == shared->c_buffer->amount())
  {
    shared->empty = true;
    event_del(shared->ev_out_writeable);
//     cerr << "DEBUG: turned OFF watching for able-to-write" << endl;
  }
}

void doQuit(int, short, void *shared_v)
{
//   cerr << "DEBUG: entering doQuit" << endl;
  SharedStuff *shared(static_cast<SharedStuff *>(shared_v));
  cerr << "Received signal to quit; " << shared->c_buffer->amount() << " bytes remaining to be sent." << endl;
  flush(shared);
}

void flush(const SharedStuff *shared)
{
  size_t remaining(shared->c_buffer->amount()), to_send_count;
  ssize_t sent_count;

  fcntl(shared->out_fd, F_SETFL, ((~O_NONBLOCK) & fcntl(shared->out_fd, F_GETFL)));
  
  while (remaining > 0)
  {
    to_send_count = shared->c_buffer->read(shared->b_buffer, BBUFFER_SIZE, NO_MODIFY);
    sent_count = write(shared->out_fd, shared->b_buffer, to_send_count);
    if (-1 == sent_count)
    {
      cerr << "WRITE ERROR! (" << strerror(errno) << ")" << endl;
      exit(1);
    }
    shared->c_buffer->read(NULL, sent_count, NO_COPY);
    remaining -= sent_count;
  }
  exit(0);
}

int main(int argc, char **argv)
{
  pipe(sig_fds);
  
  event_base *eb(event_base_new());
  CircularByteBuffer buffer(CBUFFER_SIZE);
  char *bytes = new char[BBUFFER_SIZE];
  SharedStuff shared({&buffer, bytes, true, NULL, STDIN_FILENO, STDOUT_FILENO});
  event *in_readable(event_new(eb, shared.in_fd, EV_READ | EV_PERSIST, &doIn, &shared)),
        *out_writeable(event_new(eb, shared.out_fd, EV_WRITE | EV_PERSIST, &doOut, &shared)),
        *quit(event_new(eb, sig_fds[0], EV_READ, &doQuit, &shared));
  
  shared.ev_out_writeable = out_writeable;
  
  fcntl(shared.in_fd, F_SETFL, (O_NONBLOCK | fcntl(shared.in_fd, F_GETFL)));
  fcntl(shared.out_fd, F_SETFL, (O_NONBLOCK | fcntl(shared.out_fd, F_GETFL)));
  fcntl(sig_fds[0], F_SETFL, (O_NONBLOCK | fcntl(sig_fds[0], F_GETFL)));
  fcntl(sig_fds[1], F_SETFL, (O_NONBLOCK | fcntl(sig_fds[1], F_GETFL)));

  // We want some signals to trigger orderly shutdown of the program (currently SIGINT, SIGTERM, and SIGPIPE).
  struct sigaction modification_sigaction;
  sigprocmask(SIG_BLOCK, NULL, &(modification_sigaction.sa_mask));
  modification_sigaction.sa_handler = &notifyQuit;
  modification_sigaction.sa_flags = 0;
//   sigaction(SIGINT, &modification_sigaction, NULL);
//   sigaction(SIGTERM, &modification_sigaction, NULL);
//   sigaction(SIGPIPE, &modification_sigaction, NULL);
  
  event_add(in_readable, NULL);
  event_add(out_writeable, NULL);
  event_add(quit, NULL);
  
//   cerr << "DEBUG: Entering event loop" << endl;
  event_base_dispatch(eb);
}
