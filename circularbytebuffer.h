// circularbytebuffer.h
// Copyright (C) 2010-2011 Andrew Munkres
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA


#ifndef _CIRCULARBYTEBUFFER_H
#define _CIRCULARBYTEBUFFER_H

#include <sys/types.h>

class CircularByteBuffer
{
  public:
    CircularByteBuffer(size_t bufsize);
    CircularByteBuffer();
    CircularByteBuffer(const CircularByteBuffer &orig);
    ~CircularByteBuffer();
    size_t amount() const;
    size_t write(const char *in, size_t max);
    size_t read(char *out, size_t max, unsigned int flags = 0);
    void clear();
    size_t maxAmount() const;
    void setTrace(bool trace);
  private:
    void delayedInit();
    size_t m_bufsize, readpoint, writepoint;
    char *buffer;
    bool m_trace;
};

const unsigned int NO_COPY = 0x1;   // Do not actually copy the characters into "out" buffer.
const unsigned int NO_MODIFY = 0x2; // Do not actually remove the characters from the CircularByteBuffer.

#endif
