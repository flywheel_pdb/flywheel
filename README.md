*flywheel* is a simple command-line utility that can add an I/O buffering stage into the middle of a Unix shell pipeline, implemented using the [libevent](http://libevent.org/) event-notification framework. This program was originally intended to prevent audio dropouts when using ALSA's *arecord* utility to record audio from line-in -- or, alternately, to record video from a Video4Linux device, or to record audio using PulseAudio's *parec* utility -- for a fairly long duration of time (e.g. one side of an audio cassette), and write it out to a file on disk, in such a way that data losses (e.g. audio skips/dropouts) will be **unlikely** to happen during the recording process.

A running *flywheel* process monitors its standard-in and standard-out file descriptors (using a libevent event-loop), and whenver possible will perform any the following two actions:
- Any time when incoming data (from the kernel) is available for this process to read from standard-in, it continuously transfers (using nonblocking I/O) that data, in chunks of size <=128KiB (this size is currently hardcoded), from standard-input into an internal fixed-size circular buffer (currently hardcoded to a size of 16MiB).
- Any time when this process's internal circular buffer is not empty **and** the kernel permits it to write data to standard-out, it it continuously transfers (using nonblocking I/O) that data, in chunks of size <=128KiB (this size is currently hardcoded), from its internal circular buffer to standard-out.

(Usage note: if running on Linux, the *flywheel* process can also be executed with real-time priority, e.g. by using the *chrt* command.)

**Warning**: because *flywheel*'s internal buffer is of fixed size, it might be possible for data to get lost (e.g. causing audio skips) if the data-source tied to standard-in (e.g. audio from line-in) can generate data in unpredictable-sized bursts which are large relative to the buffer's size; if that happens, you might try increasing the hardcoded buffer size beyond 16MiB. Data loss can also happen if the data-source tied to standard-in generates data faster than the data-sink tied to standard-out (e.g. the hard disk) is capable of consuming data; in that situation, *flywheel* can't help you (unless the **total duration** of the "recording" is short relative to the buffer's size).

The shellscript example below -- which is based on [this set of instructions](https://askubuntu.com/questions/60837/record-a-programs-output-with-pulseaudio) -- demostrates how to use *flywheel* to capture in real time the stream of audio output from any desktop app that uses PulseAudio to play audio (such as a web browser) and save the resulting audio stream to a file on disk:

    pulseaudio -k
    # start playing audio in program
    pactl load-module module-null-sink sink_name=mycaptureB
    pacmd list-sink-inputs
    # find the index listed for your program
    pactl move-sink-input 1 mycaptureB
    chrt -r 99 parec --device=mycaptureB.monitor --format=float32le --rate=48000 --channels=2 | chrt -r 99 ./tempfly/flywheel | cat - > rawaudiooutputfile.bin

**Important note**: the use of *chrt* in the above example won't work as-in *unless* you grant your Linux user account permission to use realtime scheduling. (An alternate option might be to use a tool like *sudo* or *pkexec* to execute the *chrt* commands as root.)

(**TODO**: add an example using ALSA arecord to record from line-in, and/or an example using PulseAudio parec to record from line-in, and/or an example of capturing video from a Video4Linux device such as a Hauppauge WinTV-PVR-USB2.)
